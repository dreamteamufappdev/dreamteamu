import React from 'react';
import { createAppContainer, createSwitchNavigator, DrawerActions } from 'react-navigation';
import { Platform, Dimensions, Text, Button} from 'react-native'
import { createStackNavigator, createDrawerNavigator} from 'react-navigation'
import { Icon } from 'react-native-elements'
import MainTabNavigator from './MainTabNavigator';

import HomeScreen from '../screens/HomeScreen'
import LinksScreen from '../screens/LinksScreen';
import TherapyScreen from '../screens/TherapyScreen';
import SettingsScreen from '../screens/SettingsScreen';

const WIDTH = Dimensions.get('window').width;


const DrawerStack = createDrawerNavigator({
  Home: HomeScreen,
  Links: LinksScreen,
  Therapy: TherapyScreen,
  Settings: SettingsScreen,

})

const DrawerNavigator = createStackNavigator({
  DrawerStack: {screen: DrawerStack}

}, {
  headerMode: 'none',
  navigationOptions: ({navigation}) => ({
    headerStyle: {backgroundColor: 'white'},
    title: 'What is poppin',
    headerTintColor: 'black',
    headerLeft: 
      <Icon
        
        name='menu'
        type='feather'
        color='black'
        onPress = {() => navigation.dispatch(DrawerActions.toggleDrawer())}
      />,
    /*headerTransparent: true,
    

    Button onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())} title="MENU" />
    
    
    */

  }),
})

const PrimaryNav = createStackNavigator({
  
  drawerStack: DrawerNavigator

}, {
  initialRouteName: 'drawerStack',
})



const App = createAppContainer(PrimaryNav);
export default App;

/*
export default createAppContainer(createSwitchNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  Main: MainTabNavigator,
}));
*/
